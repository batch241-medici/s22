// console.log("Hello World!");

// Non-Mutator Methods
/*
	- Non-Mutator methods are functions that do not mofidy or change an array after they have been created
	- These methods do not manipulate the original array
	- These methods perform tasks such as returning (or getting) elements from an array, combining arrays, and printing the output
	- indexOf(), lastIndexOf(), slice(), toString(), concat(), join()
*/

console.log('Non-Mutator Method:');
let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];
console.log(countries);
console.log("");

// indexOf()
/*
	- Returns the index number of the "first matching element" found in an array
	- If no match has been found, the result will be -1
	- The search process will be done from the first element proceeding to the last element.
	- Syntax:
		arrayName.indexOf(searchValue);
		arrayName.indexOf(searchValue, fromIndex);
*/

console.log('indexOf Method:');
let firstIndex = countries.indexOf('PH');
console.log('Result of indexOf method: ' + firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log('Result of indexOf method: ' + invalidCountry);
console.log("");

// lastIndexOf()
/*
	- Returns the index number of the "last matching item" found in the array
	- The search process will be done form the last element proceeding to the first element.
	- Syntax:
		arrayName.lastIndexOf(searchValue);
		arrayName.lastIndexOf(searchValue, fromIndex);
*/

console.log('lastIndexOf Method:');

// Getting the index number starting from the last element
let lastIndex = countries.lastIndexOf('PH');
console.log('Result of lastIndexOf method: ' + lastIndex);

// Getting the index number starting from the specified index
let lastIndextart = countries.lastIndexOf('PH', 6);
let lastIndextarter = countries.lastIndexOf('PH', 4);
console.log('Result of lastIndexOf method: ' + lastIndextart);
console.log('Result of lastIndexOf method: ' + lastIndextarter);
console.log("");

// slice()
/*
	- Slices elements from an array AND returns a new array
	- Syntax:
		arrayName.slice(startingIndex);
		arrayName.slice(startingIndex, endingIndex)
*/

console.log('slice method:');

// Slicing off elements from a specified index to the last element
let slicedArrayA = countries.slice(2);
console.log('Return from slice method:');
console.log(slicedArrayA);

// Slicing off elements from a specified index to another index

let slicedArrayB = countries.slice(1, 3);
console.log('Return from slice method:');
console.log(slicedArrayB);

// Slicing off elements starting from the last element of an array

let slicedArrayC = countries.slice(-1);
let slicedArrayD = countries.slice(-3);
let slicedArrayE = countries.slice(-5);
console.log('Result from slice method:');
console.log(slicedArrayC);
console.log(slicedArrayD);
console.log(slicedArrayE);
console.log("");

// toString()
/*
	- Return an array as a string seperated by commas
	-	Syntax:
		arrayName.toString();
*/
console.log('toString() Method:');

let stringArray = countries.toString();
console.log('Result from toString method:');
console.log(stringArray);
console.log("");

// concat()
/*
	- Combines two arrays and returns the combined result
	- Syntax:
		arrayA.concat(arrayB);
		arrayA.concat(elementA, elementB);
*/

console.log('concat() method:');
let taskArrayA = ["drink HTML", "eat javascript"];
let taskArrayB = ["inhale CSS", "breathe sass"];
let taskArrayC = ["get git", "be node"];

let tasks = taskArrayA.concat(taskArrayB);
console.log('Result from concat method:');
console.log(tasks);

// Combining multiple arrays
console.log('Result from concat method:');
let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(allTasks);

console.log('Rearranged:');
let allTasksA = taskArrayB.concat(taskArrayC, taskArrayA);
console.log(allTasksA);

// Combining arrays with elements
let combinedTasks = taskArrayA.concat('smell express', 'throw react');
console.log('Result from concat method:');
console.log(combinedTasks);
console.log("");

// join()
/*
	- Returns an array as a string separated by specified separator string
	- Syntax:
		arrayName.join("separatorString");
*/

console.log('join() Method:');

let users = ['John', 'Jane', 'Joe', 'Robert'];
console.log(users.join());
console.log(users.join(""));
console.log(users.join(' - '));
console.log(users.join(' & '));

console.log("");

console.log("--end of non-mutator methods--");

console.log('Iteration Methods');


// Iteration Methods
/*
	- Iteration methods are loops designed to perform repetitive tasks on arrays
	- Useful for manipulating array data resulting in complex tasks.
*/

// foreach()
/*
	- Similar to the for loop that iterates on each array element
	- Variable names for arrays are usually written in the plural form of the data stored in an array
	- It is common practice to use the singular form of the array content for parameter names used in array loops.
	- Array iteration methods normally work with a function supplied as an argument
	- Syntax:
		arrayName.forEach(function(individualElement) {statement});
*/

allTasks.forEach(function(task) 
	{console.log(task)
	});

let students = ['Jeru', 'Annejanette', 'Glyn', 'Jake', 'Gabryl', 'Daniel'];

students.forEach(function (student)
	{console.log("Hi, " + student + "!");
	});

// Using forEach with conditional statements
console.log('Filtered forEach:');

let filteredTasks = [];

allTasks.forEach(function (task) {
	if (task.length >= 10) {
		console.log(task);
		filteredTasks.push(task);
	}
});

console.log('Result of filtered tasks:');
console.log(filteredTasks);

students.forEach(function(student) {

	if (student.length <= 4) {
		console.log("Sa kanan ng room si: " + student);
	} else {
		console.log("Sa kaliwa ng room uupo si: " + student);
	}
})

console.log("");

// map()
/*
	- It iterates of each element AND returns new array with different values depending on the reuslt of the function's operation
	- this is useful for performing tasks where mutating/changing the elements are required
	- Unlike the forEach method, the map method requires the use of a "return" statement in order to create another array with the performed operation
	- Syntax:
		let / const resultArray = arrayName.map(function(individualElement))
*/

console.log("Map Method: ");

let numbers = [1, 2, 3, 4, 5];
console.log("Before the map method: " + numbers);

let numbersMap = numbers.map(function (number) {
	return number * number
})

console.log("Result of map method: ");
console.log(numbersMap);
console.log("");

// every()
/*
	- Checks if all element in an array meet the given condition
	- This is useful for validating data stored in arrays especially when dealing with large amounts of data.
	- Returns the true value if all elements meet the condition and false if otherwise
	- Syntax:
		let / const resultArray = arrayName.every(function(element) {
			return expression / condition
		})
*/

console.log('every() Method:');

let allValid = numbers.every(function (number) {
	return (number < 3);
});

console.log("Result of every method: ");
console.log(allValid);
console.log("");

// some()
/*
	- Checks if atleast one element in the array meets the given conditon
	- Returns a true value if atleast one element meets the condition and false if otherwise
	- Syntax:
		let / const resultArray = arrayName.some(function (element {
		return expression / condition
		}))
*/

console.log("some() method:");

let someValid = numbers.some(function(number) {
	return (number < 2);
})

console.log("Result of some method: ");
console.log(someValid);

// Combining the returned result from the every/some method may be used in other statements to perform consecutive

if (someValid) {
	console.log("Some numbers in the array are greater than 2");
}

// filter()
/*
	- Returns new array that contains elements which meets the given condition
	- Return an empty array if no elements are found
	- Useful for filtering array elements with a given condition and shortens the syntax compared to using other array iteration methods.
	- Syntax:
		let / const resultArray = arrayName.filter(function(element) {
			return expression / condition
		});
*/

console.log('filter() method: ');

let filterValid = numbers.filter(function (number) {
	return (number < 3)
})

console.log('Result of the filter method: ');
console.log(filterValid);

let nothingFound = numbers.filter(function (number) {
	return (number = 0)
});
console.log(nothingFound);

// Filtering using forEach
let filteredNumbers = [];

numbers.forEach(function (number) {
	console.log(number)

	if (number < 3) {
		filteredNumbers.push(number);
	}
})

console.log('Result of filter method: ');
console.log(filteredNumbers)
console.log("");

// includes() method
/*
	- Methods can be "chained" by using them one after another
	- The result of the first method is used on the second method until all chained methods have been solved.
*/

console.log(' includes() method: ');

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let filteredProducts = products.filter(function(product){
	return product.toLowerCase().includes("a");
})

console.log(filteredProducts);
console.log("");

// reduce()
/*
	- Evaluates statements from left to right and returns / reduces the array into a single value.
	- Syntax:
		let / const resultArr = arrayName.reduce(function(accumulator, currentValue)) {
			return expression / condition
		}
	- The "accumulator" parameter in the function stores the result for every iteration of the loop
	- The "currentValue" is the current / next element in the array that is evaluated in each iteration of the loop
	- How the "reduce" method works:
		1. The first/result element in the array is stores in the "accumulator" parameters
		2. The second/next element in the array is stores in the "currentValue"
		3. An operation is performed on the two elements
		4. The loop repeats steps 1 to 3 until all elements have been worked on.
*/

let iteration = 0;

let reducedArray = numbers.reduce(function(x, y) {
	console.warn('current iteration: ' + ++iteration);
	console.log('accumulator: ' + x);
	console.log('currentValue: ' + y);

	// The operation to reduce the array into a single value
	return x + y;
});

console.log('Result of reduce method: '  + reducedArray);

// Reducing string arrays
let list = ['Hello', 'Again', 'World'];

let reducedJoin = list.reduce(function(x, y) {
	return x + '' + y;
});
console.log('Result of reduce method: ' + reducedJoin);
